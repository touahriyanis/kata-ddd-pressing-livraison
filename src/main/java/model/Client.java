package model;

import lombok.AllArgsConstructor;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Random;

@AllArgsConstructor
public class Client {
    private ClientId id;
    private String nom;
    private Double solde;
    private List<Promotion> promotions;

    public void ajouterPromotion(Double montantCommande) {
        byte[] array = new byte[5];
        new Random().nextBytes(array);
        String promotionCode = new String(array, StandardCharsets.UTF_8);

        Promotion promotion = new Promotion(LocalDate.now().plusDays(10), promotionCode, montantCommande/10);

        this.promotions.add(promotion);
    }

    public void rembourser(Double montant) {
        solde  += montant;
    }

    public Double getSolde() {
        return solde;
    }

    public List<Promotion> getPromotions() {
        return promotions;
    }

    public ClientId getId() {
        return id;
    }
    public String getNom() {
        return nom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommandeId that = (CommandeId) o;
        return Objects.equals(id, that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

