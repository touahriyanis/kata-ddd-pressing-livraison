package model;

public class ClientId {
    private final String id;

    public ClientId(String s) {
        id = s;
    }

    public String getId() {
        return id;
    }
}
