package model;

import lombok.Builder;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

@Builder
public class Commande {
    private final CommandeId id;

    private Date dateDeCreation;

    private CommandeInformation information;

    private Status etat;

    private Date dateRecuperationLivreur;

    private Date dateRecuperationClient;

    private Client client;

    private Creneau creneau;

    public void livre() {
        etat = Status.TERMINE;
    }

    public boolean estLivreDansLesDelais(LocalDate deliveryTime) {
        return !(creneau.verifierSiHorsDelai(deliveryTime));
    }

    public void rembourser() {
        client.rembourser(information.getMontantTotal());
        etat = Status.REMBOURSE;
        client.ajouterPromotion(information.getMontantTotal());
    }

    public Status getEtat() {
        return etat;
    }

    public Client getClient() {
        return client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommandeId that = (CommandeId) o;
        return Objects.equals(id, that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}