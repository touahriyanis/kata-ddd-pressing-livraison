package model;

public class CommandeId {
    private final String id;

    public CommandeId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
