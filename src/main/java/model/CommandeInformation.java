package model;

public class CommandeInformation {
    private final String optionLivraison;

    private final Double montantTotal;

    public CommandeInformation(String optionLivraison, Double montantTotal) {
        this.optionLivraison = optionLivraison;
        this.montantTotal = montantTotal;
    }

    public boolean eligiblePourPromotion() {
        return montantTotal > 200;
    }

    public String getOptionLivraison() {
        return optionLivraison;
    }

    public Double getMontantTotal() {
        return montantTotal;
    }
}
