package model;

import model.Commande;

public interface CommandeRepository {
    void sauvegarder(Commande commande);
}
