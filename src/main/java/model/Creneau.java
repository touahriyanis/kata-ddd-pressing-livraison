package model;

import lombok.AllArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
public class Creneau {
    private final LocalDate dateDebut;

    private final LocalDate dateFin;

    public boolean verifierSiHorsDelai(LocalDate deliveryTime) {
        return deliveryTime.isAfter(dateFin);
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }
}
