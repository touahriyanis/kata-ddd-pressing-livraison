package model;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.Random;

public class Promotion {
    private final LocalDate dateFin;

    private final String code;

    private final Double remise;

    public Promotion(LocalDate dateFin, String code, Double remise) {

        this.dateFin = dateFin;
        this.code = code;
        this.remise = remise;
    }

    public boolean promotionEligiblePourCommande(int montantTotalCommande) {
        return !promotionExpire() && montantTotalCommande/2.0 < remise;
    }

    private boolean promotionExpire() {
        return dateFin.isBefore(LocalDate.now());
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public String getCode() {
        return code;
    }

    public Double getRemise() {
        return remise;
    }

}
