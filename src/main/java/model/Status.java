package model;

public enum Status {
    CREE,
    EN_COURS,
    TERMINE,
    REMBOURSE
}
