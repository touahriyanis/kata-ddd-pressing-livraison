package use_case.commande;

import model.Commande;
import model.CommandeRepository;

import java.time.LocalDate;

public class LivrerCommande {
    private final CommandeRepository commandeRepository;

    public LivrerCommande(CommandeRepository commandeRepository) {
        this.commandeRepository = commandeRepository;
    }

    public Commande delivrerCommande(Commande commande, LocalDate deliveryTime) {
        if (commande.estLivreDansLesDelais(deliveryTime)) {
            commande.livre();
        } else {
            commande.rembourser();
        }

        commandeRepository.sauvegarder(commande);

        return commande;
    }
}
