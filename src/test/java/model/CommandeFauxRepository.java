package model;

import java.util.*;

public class CommandeFauxRepository implements CommandeRepository {

    private final List<Commande> commandes = new LinkedList<>();

    @Override
    public void sauvegarder(Commande commande) {
        commandes.add(commande);
    }
}
