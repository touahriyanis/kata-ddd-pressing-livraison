package use_case;

import model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import use_case.commande.LivrerCommande;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CommandeDoit {
    private LivrerCommande livreurCommandes;
    private CommandeRepository gestionnaireCommandes;

    @BeforeEach
    public void initialiser() {
        gestionnaireCommandes = new CommandeFauxRepository();
        livreurCommandes = new LivrerCommande(gestionnaireCommandes);
    }

    @Test
    void livrer_dans_les_delais_livre_la_commande_et_change_le_status() {
        Commande commande = getCommande();

        livreurCommandes.delivrerCommande(commande, LocalDate.parse("2022-01-08"));

        assertThat(commande.getEtat()).isEqualTo(Status.TERMINE);
    }


    @Test
    void livrer_hors_delais_devrait_rembourser_et_changer_le_status() {
        Commande commande = getCommande();

        livreurCommandes.delivrerCommande(commande, LocalDate.parse("2022-01-11"));

        assertThat(commande.getEtat()).isEqualTo(Status.REMBOURSE);

        assertThat(commande.getClient().getSolde()).isEqualTo(300.0);

        assertThat(commande.getClient().getPromotions().size()).isEqualTo(1);
        assertThat(commande.getClient().getPromotions().get(0).getRemise()).isEqualTo(20);
    }

    private Commande getCommande() {
        return Commande.builder()
                .client(new Client(new ClientId("1"),"Nicolas",100.0, new LinkedList<>()))
                .creneau(new Creneau(LocalDate.parse("2022-01-08"), LocalDate.parse("2022-01-10")))
                .information(new CommandeInformation("Fast", 200.0))
                .etat(Status.CREE)
                .build();
    }

}
